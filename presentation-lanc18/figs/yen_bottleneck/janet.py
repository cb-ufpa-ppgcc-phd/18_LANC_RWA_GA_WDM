import sys
#reload(sys)  
#sys.setdefaultencoding('utf8') # for plot in PT_BR

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib.ticker import EngFormatter

def plot_graph(routes):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# define vertices or nodes as points in 2D cartesian plan
	nsfnodes = [\
			(1.50, 4.00), # 0
			(1.00, 3.00), # 1
			(2.00, 3.00), # 2
			(1.00, 2.00), # 3
			(2.00, 2.00), # 4
			(1.00, 1.00), # 5
			(2.00, 1.00)  # 6
	]

	nsflabels = ['GLA', 'MAN', 'LEE', 'BIR', 'NOT', 'BRI', 'LON' ]

	# define links or edges as node index ordered pairs in cartesian plan
	nsflinks = [\
			(0,1), (0,2),                # 0
			(1,2), (1,3),                # 1
			(2,4),                       # 2
			(3,4), (3,5), #(3,6),        # 3
			(4,6),                       # 4
			(5,6)                        # 5
	]

	# grid
	#ax.grid()

	# draw edges before vertices
	flag = False
	for link in nsflinks:
		x = [ nsfnodes[link[0]][0], nsfnodes[link[1]][0] ]
		y = [ nsfnodes[link[0]][1], nsfnodes[link[1]][1] ]
		for r in routes:
			for i in range(len(r)-1):
				xr = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				yr = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				if  (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[1] and yr[1]==y[0]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[1] and yr[1]==y[0]):
					flag = True
					break
		if not flag:
			plt.plot(x, y, '--', linewidth=1, color=[0.3, 0.3, 0.3])
		flag = False

	# highlight in red the shortest path with wavelength(s) available
	istyle = 0
	styles = ['-', '-', '-']
	colors = ['r','b','g']
	if routes:
		for r in routes:
			for i in range(len(r)-1):
				x = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				y = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				dx = (x[1]-x[0])
				dy = (y[1]-y[0])
				factor = 0.10*0.8/np.sqrt(dx**2 + dy**2)
				dx -= dx*factor
				dy -= dy*factor
				if x==[2.0,2.0] and y==[2.0,1.0]: 
					if colors[istyle]=='r': 
						plt.arrow(x[0]+0.02, y[0], dx, dy, ec=colors[istyle], fc='w', 
							linewidth=2.5, linestyle=styles[istyle],
							head_width=0.025, length_includes_head=False)
					else: 
						plt.arrow(x[0]-0.02, y[0], dx, dy, ec=colors[istyle], fc='w', 
							linewidth=2.5, linestyle=styles[istyle],
							head_width=0.025, length_includes_head=False)
				else:
					plt.arrow(x[0], y[0], dx, dy, ec=colors[istyle], fc='w', 
							linewidth=2.5, linestyle=styles[istyle],
							head_width=0.025, length_includes_head=False)
			istyle += 1

	# draw vertices
	i = 0
	for node in nsfnodes:
		# parameter to adjust text on the center of the vertice
		if i < 10:
			corr = 0.060
		else:
			corr = 0.110

		xcorr = 0.080
		ycorr = 0.080
		for r in routes:
			plt.plot(node[0], node[1], 'wo', markersize=34)
			#ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr))
			ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr))

		i += 1

	# draw source and destination
	xcorr = 0.080
	ycorr = 0.080
	i, node = 1, nsfnodes[1]
	plt.plot(node[0], node[1], 'ko', markersize=34, markeredgewidth=3.0)
	#ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr), color='white')
	ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr), color='white')

	i, node = 6, nsfnodes[6]
	plt.plot(node[0], node[1], 'ko', markersize=34, markeredgewidth=3.0)
	#ax.annotate(str(i), xy=(node[0]-corr*2, node[1]-corr), color='white')
	ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr), color='white')

	plt.xticks(np.arange(0, 6, 1))
	plt.yticks(np.arange(0, 5, 1))
	plt.xlim([0, 3.0])
	plt.ylim([0, 4.5])
	plt.show(block=False)

if __name__=='__main__':
	plot_graph([[1,2,4,6], [1,3,4,6]])
	input('Press enter')
