import sys
#reload(sys)  
#sys.setdefaultencoding('utf8') # for plot in PT_BR

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib.ticker import EngFormatter

def plot_graph(routes):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# define vertices or nodes as points in 2D cartesian plan
	nsfnodes = [\
			(2.00, 6.00), #  0
			(1.00, 6.00), #  1
			(1.00, 4.50), #  2
			(1.00, 2.50), #  3
			(1.00, 1.00), #  4
			(2.00, 1.00), #  5
			(1.50, 1.70), #  6
			(3.00, 1.00), #  7
			(4.00, 1.00), #  8
			(5.00, 3.50), #  9
			(5.00, 1.00), # 10
			(4.00, 6.00), # 11
			(5.00, 6.00)  # 12
	]

	# define links or edges as node index ordered pairs in cartesian plan
	nsflinks = [\
			(0,1), (0,5), (0,8), (0,11),  #  0
			(1,2),                        #  1
			(2,3),                        #  2
			(3,4),                        #  3
			(4,5),                        #  4
			(5,6), (5,7), (5,11),         #  5
			(7,8),                        #  7
			(8,9), (8,11),                #  8
			(9,10), (9,11),               #  9
			(11,12)                       # 11
	]

	nsflabels = ['US', 'MX', 'GT', 'SV', 'CR', 'PN', 'VE', 'CO', 'CL', 'AR', 'UY', 'BR', 'UK']

	# grid
	#ax.grid()

	# draw edges before vertices
	flag = False
	for link in nsflinks:
		x = [ nsfnodes[link[0]][0], nsfnodes[link[1]][0] ]
		y = [ nsfnodes[link[0]][1], nsfnodes[link[1]][1] ]
		for r in routes:
			for i in range(len(r)-1):
				xr = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				yr = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				if  (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[1] and yr[1]==y[0]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[1] and yr[1]==y[0]):
					flag = True
					break
		if not flag:
			plt.plot(x, y, '--', linewidth=1, color=[0.3, 0.3, 0.3])
		flag = False

	# highlight in red the shortest path with wavelength(s) available
	istyle = 0
	styles = ['-', '-', '-']
	colors = ['r','b','g']
	if routes:
		for r in routes:
			for i in range(len(r)-1):
				x = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				y = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				dx = (x[1]-x[0])
				dy = (y[1]-y[0])
				factor = 0.18*0.81215762017/np.sqrt(dx**2 + dy**2)
				dx -= dx*factor
				dy -= dy*factor
				#plt.plot(x, y, 'k', linewidth=2.5, linestyle=styles[istyle])
				plt.arrow(x[0], y[0], dx, dy, ec=colors[istyle], fc='w', 
						linewidth=2.5, linestyle=styles[istyle],
						head_width=0.125, length_includes_head=False)
			istyle += 1

	# draw vertices
	i = 0
	for node in nsfnodes:
		# parameter to adjust text on the center of the vertice
		if i < 10:
			corr = 0.060
		else:
			corr = 0.110

		xcorr = 0.110
		ycorr = 0.080
		for r in routes:
			plt.plot(node[0], node[1], 'wo', markersize=25)
			#ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr))
			ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr))

		i += 1

	# draw source and destination
	xcorr = 0.110
	ycorr = 0.080
	i, node = 3, nsfnodes[3]
	plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
	#ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr), color='white')
	ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr), color='white')

	i, node = 9, nsfnodes[9]
	plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
	#ax.annotate(str(i), xy=(node[0]-corr*2, node[1]-corr), color='white')
	ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr), color='white')

	plt.xticks(np.arange(0, 7, 1))
	plt.yticks(np.arange(0, 8, 1))
	plt.show(block=False)

if __name__=='__main__':
	plot_graph([[3,4,5,11,9], [3,2,1,0,8,9]])
	x = input('Press enter')
