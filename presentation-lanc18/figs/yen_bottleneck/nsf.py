import sys
#reload(sys)  
#sys.setdefaultencoding('utf8') # for plot in PT_BR

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib.ticker import EngFormatter

def plot_graph(routes):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# define vertices or nodes as points in 2D cartesian plan
	nsfnodes = [\
		(0.70, 2.70), # 0
		(1.20, 1.70), # 1
		(1.00, 4.00), # 2
		(3.10, 1.00), # 3
		(4.90, 0.70), # 4
		(2.00, 2.74), # 5
		(2.90, 2.66), # 6
		(3.70, 2.80), # 7
		(4.60, 2.80), # 8
		(5.80, 3.10), # 9
		(5.50, 3.90), # 10
		(6.60, 4.60), # 11
		(7.40, 3.30), # 12
		(6.50, 2.40), # 13
	]

	# define links or edges as node index ordered pairs in cartesian plan
	nsflinks = [\
		(0,1), (0,2), (0,5),  # 0
		(1,2), (1,3),         # 1
		(2,8),                # 2
		(3,4), (3,6), (3,13), # 3
		(4,9),                # 4
		(5,6), (5,10),        # 5
		(6,7),                # 6
		(7,8),                # 7
		(8,9),                # 8
		(9,11), (9,12),       # 9
		(10,11), (10,12),     # 10
		(11,13)               # 11
	]

	# grid
	#ax.grid()

	# draw edges before vertices
	flag = False
	for link in nsflinks:
		x = [ nsfnodes[link[0]][0], nsfnodes[link[1]][0] ]
		y = [ nsfnodes[link[0]][1], nsfnodes[link[1]][1] ]
		for r in routes:
			for i in range(len(r)-1):
				xr = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				yr = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				if  (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[1] and yr[1]==y[0]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[1] and yr[1]==y[0]):
					flag = True
					break
		if not flag:
			plt.plot(x, y, '--', linewidth=1, color=[0.3, 0.3, 0.3])
		flag = False

	# highlight in red the shortest path with wavelength(s) available
	istyle = 0
	styles = ['-', '-', '-']
	colors = ['r','b','g']
	if routes:
		for r in routes:
			for i in range(len(r)-1):
				x = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				y = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				dx = (x[1]-x[0])
				dy = (y[1]-y[0])
				factor = 0.12*0.81215762017/np.sqrt(dx**2 + dy**2)
				dx -= dx*factor
				dy -= dy*factor
				#plt.plot(x, y, 'k', linewidth=2.5, linestyle=styles[istyle])
				plt.arrow(x[0], y[0], dx, dy, ec=colors[istyle], fc='w', 
						linewidth=2.5, linestyle=styles[istyle],
						head_width=0.125, length_includes_head=False)
			istyle += 1

	# draw vertices
	i = 0
	for node in nsfnodes:
		# parameter to adjust text on the center of the vertice
		if i < 10:
			corr = 0.060
		else:
			corr = 0.110

		for r in routes:
			plt.plot(node[0], node[1], 'wo', markersize=25)
			ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr))

		i += 1

	# draw source and destination
	corr = 0.060
	i, node = 0, nsfnodes[0]
	plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
	ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr), color='white')

	i, node = 12, nsfnodes[12]
	plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
	ax.annotate(str(i), xy=(node[0]-corr*2, node[1]-corr), color='white')

	plt.xticks(np.arange(0, 9, 1))
	plt.yticks(np.arange(0, 7, 1))
	plt.show(block=False)

if __name__=='__main__':
	plot_graph([[0,5,10,12], [0,2,8,9,12]])
	input('Press enter')
