import sys
#reload(sys)  
#sys.setdefaultencoding('utf8') # for plot in PT_BR

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib.ticker import EngFormatter

def plot_graph(routes):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# define vertices or nodes as points in 2D cartesian plan
	nsfnodes = [\
		(5.00,  3.25), #  0
		(5.50,  3.75), #  1
		(8.25,  3.75), #  2
		(4.00,  5.00), #  3
		(9.00,  3.00), #  4
		(3.00,  3.00), #  5
		(9.00,  4.00), #  6
		(9.50,  5.00), #  7
		(10.50, 5.00), #  8
		(10.50, 3.00), #  9
		(10.50, 1.00), # 10
		(9.50,  1.00), # 11
		(9.00,  2.00), # 12
		(8.00,  2.00), # 13
		(7.00,  2.00), # 14
		(6.00,  2.00), # 15
		(6.00,  1.00), # 16
		(4.00,  1.00), # 17
		(2.00,  1.00), # 18
		(6.00,  5.50), # 19
		(1.00,  1.00), # 20
		(1.00,  2.00), # 21
		(2.00,  2.00), # 22
		(2.00,  4.00), # 23
		(2.00,  5.00), # 24
		(3.00,  5.00), # 25
		(1.00,  5.00), # 26
		(1.00,  4.00)  # 27
	]

	# define links or edges as node index ordered pairs in cartesian plan
	nsflinks = [\
			(0,1),                                #  0
			(1,3), (1,4),                         #  1
			(2,4),                                #  2
			(3,4), (3,7), (3,17), (3,19), (3,25), #  3
			(4,6), (4,12),                        #  4
			(5,25),                               #  5
			(6,7),                                #  6
			(7,8), (7,11), (7,18), (7,19),        #  7
			(8,9),                                #  8
			(9,10),                               #  9
			(10,11),                              # 10
			(11,12), (11,13), (11,15),            # 11
			(13,14),                              # 13
			(14,15),                              # 14
			(15,16), (15,19),                     # 15
			(16,17),                              # 16
			(17,18),                              # 17
			(18,19), (18,20), (18,22),            # 18
			(20,21),                              # 20
			(21,22),                              # 21
			(22,23),                              # 22
			(23,24),                              # 23
			(24,25), (24,26),                     # 24
			(26,27)                               # 26
	]

	nsflabels = [ 'RR', 'AM', 'AP', 'DF', 'PA', 'TO', 'MA', 'CE', 'RN', 'PB',
	'PB', 'PE', 'PI', 'AL', 'SE', 'BA', 'ES', 'RJ', 'SP', 'MG', 'SC', 'RS',
	'PR', 'MS', 'MT', 'GO', 'RO', 'AC' ]

	# grid
	#ax.grid()

	# draw edges before vertices
	flag = False
	for link in nsflinks:
		x = [ nsfnodes[link[0]][0], nsfnodes[link[1]][0] ]
		y = [ nsfnodes[link[0]][1], nsfnodes[link[1]][1] ]
		for r in routes:
			for i in range(len(r)-1):
				xr = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				yr = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				if  (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[0] and yr[1]==y[1]) or \
				    (xr[0]==x[0] and xr[1]==x[1] and yr[0]==y[1] and yr[1]==y[0]) or \
				    (xr[0]==x[1] and xr[1]==x[0] and yr[0]==y[1] and yr[1]==y[0]):
					flag = True
					break
		if not flag:
			plt.plot(x, y, '--', linewidth=1, color=[0.3, 0.3, 0.3])
		flag = False

	# highlight in red the shortest path with wavelength(s) available
	istyle = 0
	styles = ['-', '-', '-']
	colors = ['r','b','g']
	if routes:
		for r in routes:
			for i in range(len(r)-1):
				x = [ nsfnodes[r[i]][0], nsfnodes[r[i+1]][0] ]
				y = [ nsfnodes[r[i]][1], nsfnodes[r[i+1]][1] ]
				dx = (x[1]-x[0])
				dy = (y[1]-y[0])
				factor = 0.20*0.81215762017/np.sqrt(dx**2 + dy**2)
				dx -= dx*factor
				dy -= dy*factor
				#plt.plot(x, y, 'k', linewidth=2.5, linestyle=styles[istyle])
				plt.arrow(x[0], y[0], dx, dy, ec=colors[istyle], fc='w', 
						linewidth=2.5, linestyle=styles[istyle],
						head_width=0.125, length_includes_head=False)
			istyle += 1

	# draw vertices
	i = 0
	for node in nsfnodes:
		# parameter to adjust text on the center of the vertice
		if i < 10:
			corr = 0.060
		else:
			corr = 0.110

		xcorr = 0.210
		ycorr = 0.080
		for r in routes:
			plt.plot(node[0], node[1], 'wo', markersize=25)
			#ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr))
			ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr))

		i += 1

	# draw source and destination
	xcorr = 0.210
	ycorr = 0.080
	i, node = 3, nsfnodes[3]
	plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
	#ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr), color='white')
	ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr), color='white')

	i, node = 11, nsfnodes[11]
	plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
	#ax.annotate(str(i), xy=(node[0]-corr*2, node[1]-corr), color='white')
	ax.annotate(nsflabels[i], xy=(node[0]-xcorr, node[1]-ycorr), color='white')

	plt.xticks(np.arange(0, 9, 1))
	plt.yticks(np.arange(0, 7, 1))
	plt.show(block=False)

if __name__=='__main__':
	plot_graph([[3,7,11], [3,4,12,11]])
	input('Press enter')
