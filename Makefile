target=sample-sigconf

all:
	pdflatex $(target).tex
	bibtex $(target).aux
	pdflatex $(target).tex
	pdflatex $(target).tex
	#dvipdf $(target).dvi
	make clean

clean:
	rm -f *.aux *.bbl *.blg *.dvi *.log *.out *.xcp *.cut
