hold off;
clear all;
close all;

load 'block_ALT_8.m';
load 'block_FIX_8.m';
load 'block_GA_8.m';
load 'block_STA_8.m';
load 'block_STF_8.m';

ALT = block_ALT_8;
FIX = block_FIX_8;
GA  = block_GA_8;
STA = block_STA_8;
STF = block_STF_8;

%figure('units','normalized','outerposition',[0 0 1 1]);
figure;
h1 = plot(mean(FIX), 'k^--', 'linewidth', 2, 'markersize', 16, 'markerfacecolor', 'k'); % Dijkstra + First Fit
hold on;
h2 = plot(mean(STF), 'mh--', 'linewidth', 2, 'markersize', 14); % Dijkstra + Graph Coloring
hold on;

h3 = plot(mean(GA),  'o--',  'linewidth', 2, 'markersize', 14, 'color', [0 0.4 0.99]); % GA + GOF
hold on;
h4 = plot(mean(STA), 's--',  'linewidth', 2, 'markersize', 14, 'markerfacecolor', 'w', 'color', [0 0.5 0]); % Yen + Graph Coloring
hold on;
h5 = plot(mean(ALT), 'r*--', 'linewidth', 1, 'markersize', 14, 'markerfacecolor', 'r'); % Yen + First Fit
hold on;
grid on;

xlim([0.8 30.2]);
ylim([-2.0 82.0]);
set(gca, 'XTick', 3:3:30, 'fontsize', 30);
set(gca, 'YTick', 0:10:100, 'fontsize', 30);
grid on;

xlabel('Load (Erlangs)', 'fontsize', 40);
ylabel('Blocking Probability (%)', 'fontsize', 40);
%title('CLARA (8 channels)');

h = legend([h2 h1 h4 h5 h3], ...
' Dijkstra + Graph Coloring',... % g
' Dijkstra + First-Fit',... % k
' Yen + Graph Coloring',... % c
' Yen + First-Fit',... % r
' Genetic Algorithm',... % b
'location', 'northwest');
set(h, 'fontsize', 32);
legend boxoff;

set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.

%%% EOF %%%
